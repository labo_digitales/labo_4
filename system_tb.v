`timescale 1 ns / 1 ps

module system_tb;
	reg clk = 1;
	always #5 clk = ~clk;
	
	reg [3:0] R_VGA;
    reg [3:0] G_VGA;
    reg [3:0] B_VGA;
    wire Hsync;
    wire Vsync;

	reg resetn = 0;
	integer num;
	initial begin
		if ($test$plusargs("vcd")) begin
			$dumpfile("system.vcd");
			$dumpvars(0, system_tb);
		end
		repeat (100) @(posedge clk);
		resetn <= 1;
		$timeformat(-9,0," ns"); // Este sí va aquí
	    num = $fopen("/home/gabriel/Desktop/Labo_Digitales/Labo4/vga.txt","w"); //path absoluto. Este sí va aquí
	end

	wire trap;
	wire [7:0] out_byte;
	wire out_byte_en;
	wire pixel_clk;
	wire [11:0] RGB;

	system uut (
		.clk        (clk        ),
		.resetn     (resetn     ),
		.trap       (trap       ),
		.out_byte   (out_byte   ),
		.out_byte_en(out_byte_en),
		.pixel_clk(pixel_clk),
		.RGB(RGB),
		.Hsync(Hsync),
		.Vsync(Vsync)
		//agregar salida del clk de 25 Mhz
	);
	
	reg [19:0] contador = 0;
	//es mejor el clk de 25 Mhz
	always @(posedge pixel_clk)begin
	   R_VGA <= RGB[11:8];
	   G_VGA <= RGB[7:4];
	   B_VGA <= RGB[3:0];
	   if (contador < 416800)begin
	       $fwrite(num,"%t: %b %b %b %b %b\n", $time, Hsync, Vsync, R_VGA, G_VGA, B_VGA);
	       contador <= contador + 1;
	   end
	   else begin
	       $fclose(num);
	       $finish;
	   end
	end
	
endmodule
