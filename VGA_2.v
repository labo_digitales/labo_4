module VGA_2(
    input clk,
    input clk25M,              //25MHz
    input control,
    input resetn,
    input [31:0] posicion,  //
    input [11:0] RGB,       //Color 0x0F0
    output reg Hsync,       //
    output reg Vsync,       //
    output reg [11:0]colorOut    //color printer   
 );
 
 
 
 RAM_SINGLE_READ_PORT # (12,24,640*480) VideoMemory(
    .Clock(clk        ),
    .iWriteEnable(control),
    .iReadAddress( rAddress),
    .iWriteAddress(wAddress),
    .iDataIn(RGB),
    .oDataOut( {oVGA_R,oVGA_G,oVGA_B})
  );
 
endmodule
