module clk25M(
    input clk,
    input resetn,
    output reg clk_pixel
);

reg clockM;

always @(posedge clk) begin
    if (resetn == 0) begin
        clockM <= 1;
    end else begin
        clockM <= ~clockM;
    end
end

always @(posedge clockM) begin
    if (resetn == 0) begin
        clk_pixel <= 1;
    end else begin
        clk_pixel <= ~clk_pixel;
    end
end

endmodule 